## Truescape Developer Application Task

### Context:

Truescape provides its clients with visual products to help communicate their projects to a range of stakeholders. One visual product is an interactive, which allows our client and their stakeholders to navigate around a virtual version of their project site and enable/display:

- various stages of the project
- various scenarios based on preset/defined parameters
- layers/overlays/labels
- media such as photography or videos

This task is to create an interactive similar to what is described above.

### Goal:

To create a browser based interactive app that can be hosted online, utilising the supplied assets depicting a proposed mine.

### Objective:

**Create an Angular application with ThreeJS integration.** The application must include 2 components (`login` and `home`) with corresponding routing.

- The `login` component should include a client logo, the Truescape logo, and a fake dummy login form.
- The `home` component is the interactive mine view, which should include:
  - A `pre-mining` stage:
    - the outer terrain files (`Terrain_Outer.bin` and `Terrain_Outer.gltf`)
    - the existing terrain files (`Terrain_Existing.bin` and `Terrain_Existing.gtlf`)
  - A `mining` stage:
    - the terrain files during the mining process (`Terrain_Year16.bin` and `Terrain_Year_16.gltf`)
    - the mine buildings and roads (`Mining_Facilities.bin` and `Mining_Facilities.gltf`). These appear when the above mining stage appears
  - A switch on the UI to toggle between the `pre-mining` and `mining` stages above
  - At least one pin/marker placed in a random location on the terrain that, when clicked, will load a modal containing either a photo carousel or video player

The interactive is:

- to be navigated predominantly by mouse control, with actions similar to Google Earth or Google Maps
- to be built using the supplied 3D models and media
- to run optimally inside a browser with the greatest detail and quality possible

### Supplied files for inclusion in the interactive:

- `3d-assets`
  - Various terrain files with associated meshes
  - Buildings and/or roads with associated meshes
  - Textures associated with the meshes
- `ui`
  - pin
  - compass
- `media`
  - photos and a video

#### Asset details:

- ##### 3D Files – Supplied in `.gltf` format:
  - `Terrain_Outer.gltf` – this is the terrain surrounding the mine site; you will see it has a hole cut out of the 3D mesh - this is where the project site will sit
  - `Terrain_Existing.gltf` – this fits into the aforementioned cutout above to complete the pre-mining stage
  - `Terrain_Year16.gltf` – this is a mining stage that also fits into the cutout
  - `Mining_Facilities.gltf` – these are the mining buildings that are to appear on top of `Terrain_Year16.gltf`
  - Note that each `.gltf` file is accompanied by a corresponding external binary `.bin` file that maps the corresponding textures to it
- ##### Aerial and building textures – Supplied as `.jpg` format
  - Each of the above terrain files has its own texture map linked to it:
    - `Terrain_Outer` --> `LowResAerial.jpg`
    - `Terrain_Existing` --> `Existing.jpg`
    - `Terrain_Year16` --> `Year16.jpg`
    - `Mining_Facilities` --> the remaining textures

### Output:

A source Angular repository and associated assets.

**Please upload the finished project to a public repository and send the link to us, or alternatively as a compressed zipped file**
